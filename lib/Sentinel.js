'use strict';

var path = require("path");
var fs = require("fs");
var _ = require("underscore");
var extend = require("node.extend");
var loadConfig = require("inherit-config");
var chokidar = require("chokidar");
var debounce = require('just-debounce');
var anymatch = require("anymatch");

var Sentinel = function(filepath, config){
  this.filepath = filepath;
  this.config = config;
};
Sentinel.prototype = {
  filepath: null,
  config: {
    path: {
      bower: "./",
      ejsdata: "./.ejsdata",
      images: "./images",
      javascripts: "./js",
      fonts: "./fonts"
    },
    dist: {
      "*": "./dist",
      "*.ejs": "./dist",
      "*.js": "./dist",
      "*.scss": "./dist",
      "*.sass": "./dist",
      "*.jpg": "./dist",
      "*.gif": "./dist",
      "*.png": "./dist"
    },
    watch: {
      build: ["/**/.*/**/*.+(js|jsx|es6|ejs|scss|sass|jpg|gif|png)"],
      reload: ["/**/*.+(html|php|js|css)"],
      ignored: [
        "/**/.*/**/_*",
        "/**/.git/**/*",
        "/**/node_modules/**/*",
        "/**/bower_components/**/*",
        "/**/.sass-cache/**/*"
      ]
    }
  },
  builders: {
    javascript: require("./Builders/Builder.JavaScript.js"),
    ejs: require("./Builders/Builder.EJS.js"),
    sass: require("./Builders/Builder.Sass.js"),
    image: require("./Builders/Builder.Image.js")
  },
  setup: function(config){
    extend(true, this.config, config);
    return this;
  },
  build: function(types){
    var self = this;
    types = _.isArray(types) && types.length > 0 ? types : [];
    var builders = _.chain(_.keys(this.builders))
      .filter(function(type){
        return types.length > 0 ? _(types).contains(type) : true;
      })
      .map(function(type){
        var Builder = self.builders[type];
        var builder = Builder ? new Builder(self.filepath, self.config.global) : null;
        builder && builder.event.on("error", function (opts) {
          var browser = Sentinel.client.browser;
          browser && browser.notify(opts.message);
        });
        return builder;
      })
      .filter(function(builder){
        return builder && builder.available(self.config);
      })
      .map(function(builder){
        return builder.exec(self.config);
      })
      .value();
    return builders.length > 0 ? Promise.all(builders) : Promise.resolve(this.filepath + ": skip a build...");
  }
};
extend(Sentinel, {
  create: function(filepath){
    var dir = /\/$/.test(filepath) ? filepath.match(/(.+)\/+$/)[1] : path.dirname(filepath);
    return loadConfig(dir + "/.sentinel", {
      default: this.prototype.config,
      inherit: true
    }).then(function(conf){
      return new Sentinel(filepath, conf);
    });
  },
  watch: function(dirpath){
    var self = this;
    var opts = {
      ignored: this.prototype.config.watch.ignored.map(function(p){return dirpath + p;}),
      ignoreInitial: true
    };
    var build_paths = this.prototype.config.watch.build.map(function(p){
      return dirpath + p;
    });
    chokidar.watch(build_paths, opts)
      .on("add", debounce(function(filepath){
        self.update(filepath, ["image"]);
      }, 200))
      .on("change", debounce(function(filepath){
        self.update(filepath);
      }, 200));
    var browser_paths = this.prototype.config.watch.reload.map(function(p){
      return dirpath + p;
    });
    chokidar.watch(browser_paths, opts)
      .on("change", function(filepath){
        self.reload(filepath);
      });
  },
  update: function(filepath, types){
    return this.create(filepath).then(function(sentinel){
      return sentinel.build(types);
    });
  },
  reload: function(filepath){
    var client = this.client;
    if( client.reloadable(filepath) ){
      client.browser.reload();
    }else if( client.streamable(filepath) ){
      client.browser.reload(filepath);
    }
  },
  build: function(dirpath, types, opts){
    var self = this;
    var reading = new Promise(function(resolve, reject){
      fs.readdir(dirpath, function(err, files){
        err ? reject(err) : resolve(files);
      });
    });
    return reading.then(function(files){
      var building = _.chain(files)
        .map(function(filepath){
          return path.join(dirpath, filepath);
        })
        .filter(function(filepath){
          if( fs.statSync(filepath).isDirectory() ){
            return opts.recursive;
          }
          var matcher = [].concat(
            self.prototype.config.watch.build,
            self.prototype.config.watch.ignored.map(function(g){return "!" + g;})
          );
          return anymatch(matcher, filepath);
        })
        .map(function(filepath){
          if( fs.statSync(filepath).isDirectory() ){
            return self.build(filepath, types, opts);
          }
          return self.create(filepath).then(function(sentinel){
            return sentinel.setup({global: opts}).build(types);
          });
        })
        .value();
      return Promise.all(building);
    }).then(function(building){
      return _.flatten(building);
    });
  },
  client: {
    browser: null,
    init: function(){
      var config = Sentinel.prototype.config.browser;
      this.browser = require("browser-sync").create();
      this.browser.init({
        proxy: config.www_host + ":" + config.www_port,
        port: config.browsersync_port,
        ui: { port: config.browsersync_ui_port },
        socket: { domain: config.endpoint_authority }
      });
    },
    reloadable: function(filepath){
      return /\.(?:html|php|js)$/.test(filepath);
    },
    streamable: function(filepath){
      return /\.css$/.test(filepath);
    }
  }
});

module.exports = Sentinel;
