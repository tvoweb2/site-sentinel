'use strict';

var gulp = require("gulp");
var extend = require("node.extend");
var imagemin = require("gulp-imagemin");
var gifsicle = require("imagemin-gifsicle");
var mozjpeg = require("imagemin-mozjpeg");
var pngquant = require("imagemin-pngquant");
var filter = require("gulp-filter");

var Builder = require("./Builder");

var ImageBuilder = function(){
  Builder.apply(this, arguments);
};
extend(ImageBuilder.prototype, Builder.prototype, {
  type: "image_src",
  pattern: /\.(?:jpg|gif|png)$/,
  exec: function(opts){
    var self = this;
    var filepath = this.src;
    var dest_dir = this.dest(opts);
    var promise = this.promise();
    gulp.src(filepath)
      .pipe(filter(function(file){
        var src = opts.path[self.type];
        return src && dest_dir && file.path.indexOf(src) !== -1 && dest_dir.indexOf(src) === -1;
      }))
      .pipe(imagemin([
        gifsicle(),
        mozjpeg({ quality: 80 }),
        pngquant(),
        imagemin.svgo()
      ], {
        verbose: true
      }))
      .on('error', function(e){
        self.process().reject(e);
        console.error(e.message);
        self.event.emit("error", { message: filepath + ":" + e.message });
      })
      .pipe(gulp.dest(dest_dir)).on("end", function(){
        self.process().resolve(self);
      });
    return promise;
  }
});

module.exports = ImageBuilder;
