'use strict';

var gulp = require("gulp");
var extend = require("node.extend");
var sass = require("gulp-sass");
sass.compiler = require('node-sass');
var cssmin = require("gulp-clean-css");
var autoprefixer = require("gulp-autoprefixer");
var through = require('through2');

var Builder = require("./Builder");

var SassBuilder = function(){
  Builder.apply(this, arguments);
};
extend(SassBuilder.prototype, Builder.prototype, {
  type: "sass",
  pattern: /\.(?:scss|sass)$/,
  exec: function(opts){
    var self = this;
    var filepath = this.src;
    var dest_dir = this.dest(opts);
    var production = opts.global.node_env === "production";
    gulp.src(filepath, {sourcemaps: !production})
      .pipe(sass({
        outputStyle: "expanded"
      }))
      .pipe(autoprefixer({grid: true}))
      .pipe(production ?
        cssmin({compatibility: "ie9", inline: ["none"]}) :
        through.obj(function(file, env, cb){
          return cb(null, file);
        })
      )
      .on('error', function(e){
        self.process().reject(e);
        console.error(e.message);
        self.event.emit("error", { message: filepath + ":" + e.message });
      })
      .pipe(gulp.dest(dest_dir, {sourcemaps:"."})).on("end", function(){
        self.process().resolve(self);
      });
    return this.promise();
  }
});

module.exports = SassBuilder;
