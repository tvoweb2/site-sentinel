'use strict';

var gulp = require("gulp");
var path = require("path");
var extend = require("node.extend");
var loadConfig = require("inherit-config");
var ejs = require("gulp-ejs");
var replace = require("gulp-replace");
var parse5 = require("parse5");
var _ = require("underscore");
var rename = require("gulp-rename");

var Builder = require("./Builder");

var EJSBuilder = function(){
  Builder.apply(this, arguments);
};
extend(EJSBuilder.prototype, Builder.prototype, {
  type: "ejs",
  pattern: /\.ejs$/,
  config: {
    ext: ".html"
  },
  exec: function(opts){
    var self = this;
    var filepath = this.src;
    var dest_dir = this.dest(opts);
    var path_opts = opts.path;
    var datapath = path.join(
      path_opts.ejsdata,
      path_opts[this.type] ? path.dirname(path.relative(path_opts[this.type], filepath)) : "",
      path.basename(filepath, ".ejs")
    );
    var promise = this.promise();
    loadConfig(datapath, {default: opts}).then(function(data){
      var config = extend({}, self.config, data && data.ejs ? data.ejs:{});
      gulp.src(filepath)
        .pipe(ejs(data, {}, config).on('error', function(e){
          self.process().reject(e);
          console.error(e.message);
          self.event.emit("error", {message: filepath + ":" + e.message});
          this.emit("end");
        }))
        .pipe(replace(/<[0-9a-zA-Z]+([\s\t\n\f\r]+[^\s\x00-\x1F\x7F"'>\/=]+([\s\t\n\f\r]*=[\s\t\n\f\r]*([^\s\t\n\f\r"'=><`]+|'[^\x00-\x08\x0B\x0E-\x1F\x7F']*'|"[^\x00-\x08\x0B\x0E-\x1F\x7F"]*"))?)*[\s\t\n\f\r]*\/?>/gi, function(tag){
          var flg = parse5.parseFragment(tag);
          var elm = flg && flg.childNodes.length > 0 ? flg.childNodes[0] : null;
          if(!elm || !elm.tagName){return tag;}
          var attr;
          switch(elm.tagName.toLowerCase()){
            case "img":
            case "script":
            case "source":
              attr = "src";
              break;
            case "link":
              attr = "href";
              break;
            case "video":
              attr = "poster";
              break;
            default:
              return tag;
          }
          var val = elm.attrs ? _.findWhere(elm.attrs, {name: attr}) : null;
          if(val){
            var url = val.value;
            if(opts.global.node_env !== "production" || !opts.global.cdn_url || /^(?:https?:)?\/\//.test(url) || !/^[0-9a-zA-Z_/:%#\$&\?\(\)~\.=\+\-!'\*,;@]+$/.test(url) ){
              return tag;
            }else if( !path.isAbsolute(url) ){
              url = path.resolve(dest_dir, url).replace(new RegExp("^("+opts.global.docroot+"|"+opts.global.workroot+")"), "");
            }
            url = opts.global.cdn_url.replace("[timestamp]", new Date()-0) + path.normalize("/" + url);
            elm.attrs[_.findIndex(elm.attrs, {name: attr})].value = url;
            tag = parse5.serialize(flg);
            tag = tag.replace(new RegExp("</"+elm.tagName+">$","i"),"");
          }
          return tag;
        }))
        .pipe(rename({extname: config.ext}))
        .pipe(gulp.dest(dest_dir)).on("end", function(){
          self.process().resolve(self);
        });
    });
    return promise;
  }
});

module.exports = EJSBuilder;
