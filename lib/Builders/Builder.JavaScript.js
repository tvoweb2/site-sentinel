'use strict';

var path = require("path");
var extend = require("node.extend");
var webpack = require("webpack");

var Builder = require("./Builder");

var JavaScriptBuilder = function(){
  Builder.apply(this, arguments);
};
extend(JavaScriptBuilder.prototype, Builder.prototype, {
  type: "javascript_src",
  pattern: /\.(?:jsx?|es6)$/,
  exec: function(opts){
    var self = this;
    var filepath = this.src;
    var dest_dir = this.dest(opts);
    var noderoot_matches = __dirname.match(new RegExp("^(" + opts.global.noderoot + "/[^/]+)"));
    var pkgroot = noderoot_matches ? noderoot_matches[0] : null;
    var compiler = webpack({
      mode: opts.global.node_env || "none",
      devtool: opts.global.node_env === "development" ? "inline-source-map" : false,
      context: pkgroot,
      entry: filepath,
      output: {
        filename: path.basename(filepath, path.extname(path.basename(filepath))) + ".js",
        path: dest_dir,
        publicPath: path.resolve("/", path.relative(opts.global.docroot, dest_dir)) + "/"
      },
      resolve: {
        modules: [
          path.join(pkgroot, "node_modules"),
          "node_modules"
        ],
        extensions: [".js", ".jsx", ".json", ".es6", ".css", ".sass", ".scss"],
        alias: opts.alias
      },
      resolveLoader: {
        modules: [
          path.join(pkgroot, "node_modules"),
          "node_modules"
        ]
      },
      module: {
        rules: [
          {
            test: /\.(?:jsx|es6)(?:$|\?)/,
            use: {
              loader: "babel-loader",
              options: {
                presets: [
                  require.resolve("@babel/preset-env"),
                  require.resolve("@babel/preset-react")
                ],
                plugins: [require.resolve("@babel/plugin-syntax-jsx")]
              }
            }
          },
          {
            test: /\.css(?:$|\?)/, use: ["style-loader", "css-loader"]
          },
          {
            test: /\.(?:sass|scss)(?:$|\?)/, use: ["style-loader", "css-loader", "sass-loader"]
          },
          {
            test: /\.jpg(?:$|\?)/,
            use: {
              loader: "url-loader",
              options: {
                limit: 8192,
                name: "assets/[sha256:hash].[ext]",
                mimetype: "image/jpg"
              }
            }
          },
          {
            test: /\.gif(?:$|\?)/,
            use: {
              loader: "url-loader",
              options: {
                limit: 8192,
                name: "assets/[sha256:hash].[ext]",
                mimetype: "image/gif"
              }
            }
          },
          {
            test: /\.png(?:$|\?)/,
            use: {
              loader: "url-loader",
              options: {
                limit: 8192,
                name: "assets/[sha256:hash].[ext]",
                mimetype: "image/png"
              }
            }
          },
          {
            test: /\.svg(?:$|\?)/,
            use: {
              loader: "url-loader",
              options: {
                limit: 8192,
                name: "assets/[sha256:hash].[ext]",
                mimetype: "image/svg+xml"
              }
            }
          },
          {
            test: /\.woff2?(?:$|\?)/,
            use: {
              loader: "url-loader",
              options: {
                limit: 8192,
                name: "assets/[sha256:hash].[ext]",
                mimetype: "application/font-woff"
              }
            }
          },
          {
            test: /\.(?:ttf|eot)(?:$|\?)/,
            use: {
              loader: "file-loader",
              options: {
                name: "assets/[sha256:hash].[ext]",
                mimetype: "application/font-woff"
              }
            }
          }
        ]
      }
    });

    compiler.run(function(e){
      if(e){
        self.process().reject(e);
        console.error(e.message);
        self.event.emit("error", { message: filepath + ":" + e.message });
      }else{
        self.process().resolve(self);
      }
    });

    return this.promise();
  }
});

module.exports = JavaScriptBuilder;
