'use strict';

var path = require("path");
var GlobKeys = require("globkeys");
var EventEmitter = require('events').EventEmitter;

var Builder = function(src, opts){
  this.src = src;
  opts && opts.umask && process.umask(opts.umask);
  this.event = new EventEmitter();
};
Builder.prototype = {
  src: null,
  type: null,
  event: null,
  pattern: null,
  processes: null,
  available: function(opts){
    return opts.path[this.type] && this.src.indexOf(opts.path[this.type]) === 0 && this.pattern && this.pattern.test(this.src);
  },
  dest: function(opts){
    var filepath = this.src;
    var distlist = new GlobKeys(opts.dist);
    var dest = distlist.resolve(filepath);
    var type_root = opts.path ? opts.path[this.type] : null;
    if(type_root){
      return path.join(dest, path.dirname(path.relative(type_root, filepath)).replace(/\.\./g,""));
    }else{
      return dest;
    }
  },
  promise: function(name){
    var self = this;
    this.processes = this.processes || {};
    name = name || "_";
    return new Promise(function(resolve, reject){
      self.processes[name] = {
        resolve: resolve,
        reject: reject
      };
    });
  },
  process: function(name){
    return this.processes[name || "_"];
  }
};

module.exports = Builder;
