#!/usr/bin/env node

'use strict';

var Sentinel = require("../lib/Sentinel");
Sentinel.prototype.setup(require(process.env.HOME + "/.sentinel.json").config);

Sentinel.client.init();
Sentinel.watch(process.cwd());
